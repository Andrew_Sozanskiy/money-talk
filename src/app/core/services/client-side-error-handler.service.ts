import { ErrorHandler, Injectable } from '@angular/core';

import * as Sentry from '@sentry/browser';

@Injectable()
export class ClientSideErrorHandler implements ErrorHandler {
  handleError(error: any) {
    throw error;
    // Sentry.captureException(error.originalError || error);
  }
}
