import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from '../../../environments/environment';

class QueryOptions {
  // todo: remove any
  [details: string]: any;
}

interface Header {
  key: string;
  value: string;
}

@Injectable({
  providedIn: 'root',
})
export class HttpWrapperService {
  constructor(private httpClient: HttpClient) {}

  get<T>(
    endpoint: Array<string>,
    headers: Array<Header> = [],
    queryOptions: QueryOptions = new QueryOptions(),
    apiUrl = environment.endpoints.apiUrl
  ): Observable<T> {
    return this.httpClient.get<T>(`${apiUrl}${endpoint.join('/')}`, {
      ...this.setHeaders(headers),
      params: queryOptions,
    });
  }

  post<T>(
    endpoint: Array<string>,
    body: T,
    headers: Array<Header> = [],
    queryOptions: QueryOptions = new QueryOptions(),
    apiUrl = environment.endpoints.apiUrl
  ): Observable<T> {
    return this.httpClient.post<T>(`${apiUrl}${endpoint.join('/')}`, body, {
      ...this.setHeaders(headers),
      params: queryOptions,
    });
  }

  put<T>(
    endpoint: Array<string>,
    body: T,
    headers: Array<Header> = [],
    apiUrl = environment.endpoints.apiUrl
  ): Observable<T> {
    return this.httpClient.put<T>(`${apiUrl}${endpoint.join('/')}`, body, {
      ...this.setHeaders(headers),
    });
  }

  private setHeaders(headers: Array<Header>): HttpHeaders {
    const httpHeaders = new HttpHeaders();
    headers.forEach(header => {
      httpHeaders.set(header.key, header.value);
    });
    return httpHeaders;
  }
}
