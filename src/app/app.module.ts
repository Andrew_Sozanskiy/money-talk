import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';

import { HttpErrorInterceptor } from '@core//interceptors/http-error-handler.interceptor';
import { ClientSideErrorHandler } from '@core//services/client-side-error-handler.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthModule } from './auth/auth.module';

import * as Sentry from '@sentry/browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthGuard } from '@globalShared//guards/auth.guard';
import { NotFoundComponent } from '@globalShared//components/not-found/not-found.component';

Sentry.init({
  dsn: 'https://2941b5754d8047858c81ac02313b1b99@sentry.io/2243270',
});

@NgModule({
  declarations: [AppComponent, NotFoundComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AuthModule,
    BrowserAnimationsModule,
  ],
  providers: [
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true,
    },
    {
      provide: ErrorHandler,
      useClass: ClientSideErrorHandler,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
