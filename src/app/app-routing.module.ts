import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

import { AuthGuard } from '@globalShared//guards/auth.guard';
import { NotFoundComponent } from '@globalShared//components/not-found/not-found.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: 'mt',
    loadChildren: './pages/pages.module#PagesModule',
    canActivate: [AuthGuard],
  },
  // {
  //   component: NotFoundComponent,
  //   path: '404',
  // },
  // {
  //   path: '**',
  //   redirectTo: '404',
  // },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
