import { Injectable } from '@angular/core';

import { UserModel } from '@globalShared//models/user.model';

const USER = 'user';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private isAuthenticated = false;

  get isLoggedIn(): boolean {
    return this.isAuthenticated;
  }

  login(user: UserModel) {
    localStorage.setItem(USER, JSON.stringify(user));
    this.isAuthenticated = true;
  }

  logout() {
    this.isAuthenticated = false;
    localStorage.clear();
  }
}
