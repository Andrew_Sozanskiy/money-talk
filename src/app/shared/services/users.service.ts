import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { HttpWrapperService } from '@core//services/http-wrapper.service';
import { UserModel } from '@globalShared//models/user.model';

const USERS = 'users';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  constructor(private httpWrapperService: HttpWrapperService) {}

  getUserByEmail(email: string): Observable<UserModel> {
    return this.httpWrapperService
      .get<UserModel[]>([USERS], [], { email })
      .pipe(
        map((user: UserModel[]) => {
          return user[0];
        })
      );
  }

  createNewUser(user: UserModel): Observable<UserModel> {
    return this.httpWrapperService.post<UserModel>([USERS], user);
  }
}
