import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ContentLoaderComponent } from './components/content-loader/content-loader.component';

const MODULES = [
  ReactiveFormsModule,
  FormsModule,
  HttpClientModule,
  NgxChartsModule,
];

@NgModule({
  imports: [...MODULES],
  exports: [...MODULES, ContentLoaderComponent],
  declarations: [ContentLoaderComponent],
})
export class SharedModule {}
