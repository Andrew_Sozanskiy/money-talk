import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BillPageComponent } from '@pages//bill-page/bill-page.component';
import { HistoryPageComponent } from '@pages//history-page/history-page.component';
import { PlanningPageComponent } from '@pages//planning-page/planning-page.component';
import { RecordsPageComponent } from '@pages//records-page/records-page.component';

import { PagesComponent } from './pages.component';
import { HistoryDetailComponent } from '@pages//history-page/history-detail/history-detail.component';
import { AuthGuard } from '@globalShared//guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: PagesComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'bill',
        component: BillPageComponent,
      },
      {
        path: 'history',
        component: HistoryPageComponent,
      },
      {
        path: 'history/:id',
        component: HistoryDetailComponent,
      },
      {
        path: 'planning',
        component: PlanningPageComponent,
      },
      {
        path: 'records',
        component: RecordsPageComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {}
