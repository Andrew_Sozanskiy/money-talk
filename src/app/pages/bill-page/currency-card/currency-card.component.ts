import { Component, Input } from '@angular/core';

import { Currencies, ExchangeModel } from '@pagesShared//models/bill.model';

@Component({
  selector: 'mt-currency-card',
  templateUrl: './currency-card.component.html',
  styleUrls: ['./currency-card.component.scss'],
})
export class CurrencyCardComponent {
  @Input() currency: ExchangeModel;

  currencies: Array<string> = [Currencies.EUR, Currencies.USD];
}
