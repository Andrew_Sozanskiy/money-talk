import { Component, OnInit } from '@angular/core';

import { BillService } from '@pagesShared//services/bill.service';
import { BillModel, ExchangeModel } from '@pagesShared//models/bill.model';

@Component({
  selector: 'mt-bill-page',
  templateUrl: './bill-page.component.html',
  styleUrls: ['./bill-page.component.scss'],
})
export class BillPageComponent implements OnInit {
  bill: BillModel;
  currency: ExchangeModel;

  isLoaded: boolean;

  constructor(private billService: BillService) {}

  async ngOnInit() {
    await this.loadBillAndCurrencyData();
  }

  async reload() {
    await this.loadBillAndCurrencyData();
  }

  private async loadBillAndCurrencyData(): Promise<void> {
    this.isLoaded = false;
    this.bill = await this.billService.getBill().toPromise();
    this.currency = await this.billService.getCurrency().toPromise();
    this.isLoaded = true;
  }
}
