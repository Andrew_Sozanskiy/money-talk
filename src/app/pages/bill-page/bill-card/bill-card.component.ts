import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';

import {
  BillModel,
  Currencies,
  ExchangeModel,
} from '@pagesShared//models/bill.model';

@Component({
  selector: 'mt-bill-card',
  templateUrl: './bill-card.component.html',
  styleUrls: ['./bill-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BillCardComponent implements OnInit {
  @Input() bill: BillModel;
  @Input() currency: ExchangeModel;

  // todo: does this right ?
  dollar: number;
  euro: number;

  ngOnInit() {
    // todo: does this right ?
    const { rates } = this.currency;
    this.dollar = rates[Currencies.USD] * this.bill.value;
    this.euro = rates[Currencies.EUR] * this.bill.value;
  }
}
