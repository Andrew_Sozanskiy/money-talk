import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { PieChartModule } from '@swimlane/ngx-charts';

import { SharedModule } from '@globalShared//shared.module';
import { BillCardComponent } from '@pages//bill-page/bill-card/bill-card.component';
import { BillPageComponent } from '@pages//bill-page/bill-page.component';
import { CurrencyCardComponent } from '@pages//bill-page/currency-card/currency-card.component';
import { HistoryChartComponent } from '@pages//history-page/history-chart/history-chart.component';
import { HistoryDetailComponent } from '@pages//history-page/history-detail/history-detail.component';
import { HistoryEventsComponent } from '@pages//history-page/history-events/history-events.component';
import { HistoryFilterComponent } from '@pages//history-page/history-filter/history-filter.component';
import { HistoryPageComponent } from '@pages//history-page/history-page.component';
import { PagesRoutingModule } from '@pages//pages-routing.module';
import { PagesComponent } from '@pages//pages.component';
import { PlanningPageComponent } from '@pages//planning-page/planning-page.component';
import { AddCategoryComponent } from '@pages//records-page/add-category/add-category.component';
import { AddEventComponent } from '@pages//records-page/add-event/add-event.component';
import { EditCategoryComponent } from '@pages//records-page/edit-category/edit-category.component';
import { RecordsPageComponent } from '@pages//records-page/records-page.component';
import { HeaderComponent } from '@pagesShared//components/header/header.component';
import { SidebarComponent } from '@pagesShared//components/sidebar/sidebar.component';
import { DropdownDirective } from '@pagesShared//directies/dropdown.directive';
import { MomentPipe } from '@pagesShared//pipes/moment.pipe';
import { BillService } from '@pagesShared//services/bill.service';
import { CategoriesService } from '@pagesShared//services/categories.service';
import { EventsService } from '@pagesShared//services/events.service';
import { MtEventsFilterPipe } from '@pagesShared//pipes/mt-events-filter.pipe';

const COMPONENTS = [
  PagesComponent,
  BillPageComponent,
  HistoryPageComponent,
  PlanningPageComponent,
  RecordsPageComponent,
  SidebarComponent,
  HeaderComponent,
  BillCardComponent,
  CurrencyCardComponent,
  AddEventComponent,
  AddCategoryComponent,
  EditCategoryComponent,
  HistoryChartComponent,
  HistoryEventsComponent,
  HistoryDetailComponent,
  HistoryFilterComponent,
];

const PIPES = [MomentPipe, MtEventsFilterPipe];

const DIRECTIVES = [DropdownDirective];

const MODULES = [
  SharedModule,
  CommonModule,
  PagesRoutingModule,
  PieChartModule,
];

const SERVICES = [BillService, CategoriesService, EventsService];

@NgModule({
  declarations: [...COMPONENTS, ...DIRECTIVES, ...PIPES],
  imports: [...MODULES],
  providers: [...SERVICES],
})
export class PagesModule {}
