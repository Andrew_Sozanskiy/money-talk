import { Component, OnInit } from '@angular/core';

import { combineLatest } from 'rxjs';

import { BillModel } from '@pagesShared//models/bill.model';
import { Category } from '@pagesShared//models/category.model';
import { MTEvent, MTEventTypes } from '@pagesShared//models/event.model';
import { BillService } from '@pagesShared//services/bill.service';
import { CategoriesService } from '@pagesShared//services/categories.service';
import { EventsService } from '@pagesShared//services/events.service';

@Component({
  selector: 'mt-planning-page',
  templateUrl: './planning-page.component.html',
  styleUrls: ['./planning-page.component.scss'],
})
export class PlanningPageComponent implements OnInit {
  isLoaded = false;

  bill: BillModel;
  categories: Category[] = [];
  mTevents: MTEvent[] = [];

  constructor(
    private billService: BillService,
    private categoriesService: CategoriesService,
    private eventsService: EventsService
  ) {}

  ngOnInit() {
    combineLatest(
      this.billService.getBill(),
      this.categoriesService.getCategories(),
      this.eventsService.getEvents()
    ).subscribe((data: [BillModel, Category[], MTEvent[]]) => {
      this.bill = data[0];
      this.categories = data[1];
      this.mTevents = data[2];
      this.isLoaded = true;
    });
  }

  getCategoryCost(category: Category): number {
    const categoryEvents = this.mTevents.filter(mtEvent => {
      return (
        category.id === mtEvent.category && mtEvent.type === MTEventTypes.OUTCOME
      );
    });
    return categoryEvents.reduce((acc, curr) => {
      return acc + curr.amount;
    }, 0);
  }

  getCategoryColorClass(category: Category): string {
    const percent = this.getPercent(category);
    return percent < 60 ? 'success' : percent >= 100 ? 'danger' : 'warning';
  }

  getCategoryPercent(category: Category): string {
    return this.getPercent(category) + '%';
  }

  getPercent(category: Category): number {
    const percent = (100 * this.getCategoryCost(category)) / category.capacity;
    return percent > 100 ? 100 : percent;
  }
}
