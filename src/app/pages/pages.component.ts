import { Component, HostBinding, OnInit } from '@angular/core';

import { fadeStateTrigger } from '@globalShared//animations/fade.animation';

@Component({
  selector: 'mt-system',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss'],
  animations: [fadeStateTrigger],
})
export class PagesComponent implements OnInit {
  @HostBinding('@fade') a = true;

  constructor() {}

  ngOnInit() {}
}
