import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { Category } from '@pagesShared//models/category.model';

@Component({
  selector: 'mt-history-filter',
  templateUrl: './history-filter.component.html',
  styleUrls: ['./history-filter.component.scss'],
})
export class HistoryFilterComponent implements OnInit {
  @Output() close = new EventEmitter();
  @Output() accept = new EventEmitter();

  @Input() categories: Category[] = [];

  timePeriods = [
    { type: 'd', label: 'День' },
    { type: 'W', label: 'Неделя' },
    { type: 'M', label: 'Месяц' },
  ];

  types = [
    { type: 'income', label: 'Доход' },
    { type: 'outcome', label: 'Расход' },
  ];

  selectedPeriod = 'd';
  selectedTypes = [];
  selectedCategories = [];

  constructor() {}

  ngOnInit() {}

  handleChangeType(event: { checked: boolean; value: string }) {
    this.calculateInputParams('selectedTypes', event.checked, event.value);
  }

  handleChangeCategory(event: { checked: boolean; value: string }) {
    this.calculateInputParams('selectedCategories', event.checked, event.value);
  }

  applyFilters() {
    this.accept.emit({
      types: this.selectedTypes,
      categories: this.selectedCategories,
      period: this.selectedPeriod,
    });
  }

  closeModal() {
    this.close.emit();
  }

  private calculateInputParams(field: string, checked: boolean, value: string) {
    if (checked) {
      // @ts-ignore
      // tslint:disable-next-line:no-unused-expression
      this[field].indexOf(value) === -1 ? this[field].push(value) : null;
    } else {
      // @ts-ignore
      this[field] = this[field].filter((el: string) => el !== value);
    }
    // @ts-ignore
    console.log(this[field]);
  }
}
