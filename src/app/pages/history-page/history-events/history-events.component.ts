import { Component, Input, OnInit } from '@angular/core';
import { MTEvent } from '@pagesShared//models/event.model';
import { Category } from '@pagesShared//models/category.model';

@Component({
  selector: 'mt-history-events',
  templateUrl: './history-events.component.html',
  styleUrls: ['./history-events.component.scss'],
})
export class HistoryEventsComponent implements OnInit {
  @Input() mtEvents: MTEvent[] = [];
  @Input() categories: Category[] = [];

  searchValue = '';
  searchField = 'amount';
  searchPlaceholder = 'Сумма';

  constructor() {}

  ngOnInit() {
    this.mtEvents.forEach(mtEvent => {
      const category = this.categories.find(
        ({ id }) => id === mtEvent.category
      );
      category
        ? (mtEvent.categoryName = category.name)
        : (mtEvent.categoryName = 'category name not found');
    });
  }

  getMtEventCssClass(mtEvent: MTEvent) {
    return {
      label: true,
      'label-danger': mtEvent.type === 'outcome',
      'label-success': mtEvent.type === 'income',
    };
  }

  changeCriteria(field: string) {
    const namesMap = {
      amount: 'Сумма',
      date: 'Дата',
      category: 'Категория',
      type: 'Тип',
    };
    // @ts-ignore
    this.searchPlaceholder = namesMap[field];
    this.searchField = field;
  }
}
