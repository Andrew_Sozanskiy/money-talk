import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Subscription } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { EventsService } from '@pagesShared//services/events.service';
import { CategoriesService } from '@pagesShared//services/categories.service';
import { MTEvent } from '@pagesShared//models/event.model';
import { Category } from '@pagesShared//models/category.model';

@Component({
  selector: 'mt-history-detail',
  templateUrl: './history-detail.component.html',
  styleUrls: ['./history-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HistoryDetailComponent implements OnInit, OnDestroy {
  mtEvent: MTEvent;
  category: Category;

  isLoading = true;

  subscription$: Subscription;

  constructor(
    private eventsService: EventsService,
    private categoriesService: CategoriesService,
    private activatedRoute: ActivatedRoute,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  get isMtEventTypeIncome(): boolean {
    return this.mtEvent && this.mtEvent.type === 'income';
  }

  get isMtEventTypeOutcome(): boolean {
    return this.mtEvent && this.mtEvent.type === 'outcome';
  }

  async ngOnInit() {
    this.subscription$ = this.activatedRoute.params
      .pipe(
        mergeMap(({ id }) => {
          return this.eventsService.getEventById(id);
        }),
        mergeMap(mtEvent => {
          this.mtEvent = mtEvent;
          return this.categoriesService.getCategoryById(mtEvent.category);
        })
      )
      .subscribe(category => {
        this.category = category;
        this.isLoading = false;
        this.changeDetectorRef.detectChanges();
      });
  }

  getMtEventCssClass() {
    return {
      'card-danger': this.isMtEventTypeOutcome,
      'card-success': this.isMtEventTypeIncome,
    };
  }

  ngOnDestroy() {
    if (this.subscription$) {
      this.subscription$.unsubscribe();
    }
  }
}
