import { Component, OnInit } from '@angular/core';

import * as moment from 'moment';

import { Category } from '@pagesShared//models/category.model';
import { MTEvent, MTEventTypes } from '@pagesShared//models/event.model';
import { CategoriesService } from '@pagesShared//services/categories.service';
import { EventsService } from '@pagesShared//services/events.service';

@Component({
  selector: 'mt-history-page',
  templateUrl: './history-page.component.html',
  styleUrls: ['./history-page.component.scss'],
})
export class HistoryPageComponent implements OnInit {
  isLoaded = false;
  isFilterVisible = false;

  // todo: add chart data type
  chartData: any = [];

  categories: Category[] = [];
  mtEvents: MTEvent[] = [];
  filteredMtEvents: MTEvent[] = [];

  constructor(
    private eventsService: EventsService,
    private categoriesService: CategoriesService
  ) {}

  async ngOnInit(): Promise<void> {
    await this.init();
  }

  openFilter() {
    this.toggleFilterVisibility(true);
  }

  onAccept(filterData: any) {
    this.toggleFilterVisibility(false);
    this.setOriginalEvents();

    const startPeriod = moment()
      .startOf(filterData.period)
      .startOf('d');
    const endPeriod = moment()
      .endOf(filterData.period)
      .endOf('d');

    this.filteredMtEvents = this.filteredMtEvents
      .filter(mtEvent => {
        return filterData.types.indexOf(mtEvent.type) !== -1;
      })
      .filter(mtEvent => {
        return (
          filterData.categories.indexOf(mtEvent.category.toString()) !== -1
        );
      })
      .filter(mtEvent => {
        const momentDate = moment(mtEvent.date, 'DD.MM.YYYY HH:mm:ss');
        return momentDate.isBetween(startPeriod, endPeriod);
      });
    this.calculateChartData(this.categories, this.filteredMtEvents);
  }

  onClose() {
    this.toggleFilterVisibility(false);
    this.setOriginalEvents();
    this.calculateChartData(this.categories, this.filteredMtEvents);
  }

  private setOriginalEvents() {
    this.filteredMtEvents = [...this.mtEvents];
  }

  private async init(): Promise<void> {
    try {
      this.categories = await this.categoriesService
        .getCategories()
        .toPromise();
      this.mtEvents = await this.eventsService.getEvents().toPromise();
      this.setOriginalEvents();
      this.calculateChartData(this.categories, this.filteredMtEvents);
      this.isLoaded = true;
    } catch (e) {
      throw e;
    }
  }

  private toggleFilterVisibility(dir: boolean) {
    this.isFilterVisible = dir;
  }

  private calculateChartData(
    categories: Array<Category>,
    events: Array<MTEvent>
  ) {
    categories.forEach(category => {
      const categoryEvents = events.filter(event => {
        return (
          event.category === category.id && event.type === MTEventTypes.OUTCOME
        );
      });
      const sum = categoryEvents.reduce((acc, curr) => {
        return acc + curr.amount;
      }, 0);
      this.chartData.push({
        name: category.name,
        value: sum,
      });
    });
  }
}
