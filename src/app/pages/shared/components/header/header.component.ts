import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '@globalShared//services/auth.service';

import * as moment from 'moment';

@Component({
  selector: 'mt-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent {
  constructor(private authService: AuthService, private router: Router) {}

  get date(): string {
    return moment().format();
  }

  get userName(): string {
    const user = localStorage.getItem('user');
    const { name } = user ? JSON.parse(user) : '';
    return name;
  }

  async logout() {
    this.authService.logout();
    await this.router.navigate(['/login']);
  }
}
