import { Component } from '@angular/core';

@Component({
  selector: 'mt-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent {}
