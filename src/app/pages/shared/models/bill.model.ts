export class BillModel {
  constructor(
    public value: number,
    public currency: CurrencyType
  ) {}
}

export class ExchangeModel {
  constructor(public base: string, public date: string, public rates: IRate) {}
}

interface IRate {
  [key: string]: number;
}

export enum Currencies {
  RUB = 'RUB',
  USD = 'USD',
  EUR = 'EUR',
}

export type CurrencyType = Currencies.EUR | Currencies.RUB | Currencies.USD;
