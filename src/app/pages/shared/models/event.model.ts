export class MTEvent {
  constructor(
    public type: string,
    public amount: number,
    public category: number,
    public date: string,
    public description: string,
    public id?: number,
    public categoryName?: string
  ) {}
}

export enum MTEventTypes {
  INCOME = 'income',
  OUTCOME = 'outcome',
}

export type MTEventType = MTEventTypes.INCOME | MTEventTypes.OUTCOME;
