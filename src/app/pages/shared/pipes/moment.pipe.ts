import { Pipe, PipeTransform } from '@angular/core';

import * as moment from 'moment';

@Pipe({
  name: 'mtMoment',
})
export class MomentPipe implements PipeTransform {
  transform(date: string, formatString: string = 'DD MM YYYY hh:mm:ss'): string {
    // @ts-ignore
    return moment(date ).format(formatString);
  }
}
