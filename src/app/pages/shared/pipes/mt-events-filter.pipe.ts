import { Pipe, PipeTransform } from '@angular/core';
import { MTEvent } from '@pagesShared//models/event.model';

@Pipe({
  name: 'mtEventsFilter',
})
export class MtEventsFilterPipe implements PipeTransform {
  transform(items: MTEvent[], value: string, field: string): MTEvent[] {
    if (items.length === 0 || !value) {
      return items;
    }

    return items.filter(item => {
      // @ts-ignore
      if (!isNaN(item[field])) {
        // @ts-ignore
        item[field] += '';
      }
      const { categoryName } = item;
      if (field === 'category' && categoryName) {
        return categoryName.toLowerCase().indexOf(value.toLowerCase()) !== -1;
      }

      if (field === 'type') {
        if ('доход'.includes(value.toLowerCase())) {
          return (
            // @ts-ignore
            item.type.indexOf('income') !== -1
          );
        } else if ('расход'.includes(value.toLowerCase())) {
          return (
            // @ts-ignore
            item.type.indexOf('outcome') !== -1
          );
        }
      }
      // @ts-ignore
      return item[field].toLowerCase().indexOf(value.toLowerCase()) !== -1;
    });
  }
}
