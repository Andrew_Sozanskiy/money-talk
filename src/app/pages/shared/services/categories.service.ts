import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { HttpWrapperService } from '@core//services/http-wrapper.service';
import { Category } from '../models/category.model';

const ENDPOINT = 'categories';

@Injectable()
export class CategoriesService {
  constructor(private httpWrapperService: HttpWrapperService) {}

  addCategory(category: Category): Observable<Category> {
    return this.httpWrapperService.post([ENDPOINT], category);
  }

  getCategories(): Observable<Array<Category>> {
    return this.httpWrapperService.get([ENDPOINT]);
  }

  updateCategory(category: Required<Category>): Observable<Category> {
    return this.httpWrapperService.put(
      [ENDPOINT, category.id.toString()],
      category
    );
  }

  getCategoryById(id: number): Observable<Category> {
    return this.httpWrapperService.get([ENDPOINT, id.toString()]);
  }
}
