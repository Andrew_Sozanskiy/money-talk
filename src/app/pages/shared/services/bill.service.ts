import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from '../../../../environments/environment';

import { HttpWrapperService } from '@core//services/http-wrapper.service';
import {
  Currencies,
  CurrencyType,
  BillModel,
  ExchangeModel,
} from '../models/bill.model';

enum ENDPOINTS {
  BILL = 'bill',
  LATEST = 'latest',
}

@Injectable()
export class BillService {
  constructor(private httpWrapperService: HttpWrapperService) {}

  getBill(): Observable<BillModel> {
    return this.httpWrapperService.get<BillModel>([ENDPOINTS.BILL]);
  }

  updateBill(bill: BillModel): Observable<BillModel> {
    return this.httpWrapperService.put([ENDPOINTS.BILL], bill);
  }

  getCurrency(
    baseCurrency: CurrencyType = Currencies.RUB
  ): Observable<ExchangeModel> {
    return this.httpWrapperService.get(
      [ENDPOINTS.LATEST],
      [],
      { base: baseCurrency },
      environment.endpoints.exchangeApiUrl
    );
  }
}
