import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { HttpWrapperService } from '@core//services/http-wrapper.service';
import { MTEvent } from '../models/event.model';

const ENDPOINT = 'events';

@Injectable()
export class EventsService {
  constructor(private httpWrapperService: HttpWrapperService) {}

  addEvent(event: MTEvent): Observable<MTEvent> {
    return this.httpWrapperService.post<MTEvent>([ENDPOINT], event);
  }

  getEvents(): Observable<Array<MTEvent>> {
    return this.httpWrapperService.get([ENDPOINT]);
  }

  getEventById(id: string): Observable<MTEvent> {
    return this.httpWrapperService.get([ENDPOINT, id]);
  }
}
