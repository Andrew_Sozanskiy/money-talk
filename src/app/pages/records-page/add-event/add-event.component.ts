import { Component, Input } from '@angular/core';
import { NgForm } from '@angular/forms';

import * as moment from 'moment';

import { BillModel } from '@pagesShared//models/bill.model';
import { Category } from '@pagesShared//models/category.model';
import {
  MTEvent,
  MTEventType, MTEventTypes,
} from '@pagesShared//models/event.model';
import { BillService } from '@pagesShared//services/bill.service';
import { EventsService } from '@pagesShared//services/events.service';

@Component({
  selector: 'mt-add-event',
  templateUrl: './add-event.component.html',
  styleUrls: ['./add-event.component.scss'],
})
export class AddEventComponent {

  constructor(
    private billService: BillService,
    private eventsService: EventsService
  ) {}
  @Input() categories: Array<Category> = [];

  mtEventTypes = MTEventTypes;

  private static isActionCanBeDenied(amount: number, bill: number): boolean {
    return amount > bill;
  }

  async onSubmit(form: NgForm): Promise<void> {
    const { amount, description, category, eventType } = form.value;
    const now = moment().format('DD MM YYYY hh:mm:ss');
    const mtEvent = new MTEvent(eventType, amount, +category, now, description);
    const newBill = await this.getNewBill(eventType, amount);
    if (newBill) {
      await this.billService
        .updateBill({
          value: newBill.value,
          currency: newBill.currency,
        })
        .toPromise();
      await this.eventsService.addEvent(mtEvent).toPromise();
      this.resetForm(form);
    }
  }

  preventMinus(): boolean {
    return false;
  }

  private resetForm(form: NgForm) {
    form.setValue({
      amount: 1,
      description: '',
      category: 1,
      eventType: this.mtEventTypes.OUTCOME,
    });
    form.form.markAsPristine();
    form.form.markAsUntouched();
  }

  private async getNewBill(
    eventType: MTEventType,
    amount: number
  ): Promise<BillModel | undefined> {
    let value = 0;
    const bill = await this.billService.getBill().toPromise();
    if (eventType === this.mtEventTypes.OUTCOME) {
      if (AddEventComponent.isActionCanBeDenied(amount, bill.value)) {
        // todo: add ang material toaster??
        return;
      } else {
        value = bill.value - amount;
      }
    } else {
      value = bill.value + amount;
    }
    return { value, currency: bill.currency };
  }
}
