import { Component, EventEmitter, Output } from '@angular/core';
import { NgForm } from '@angular/forms';

import { Category } from '@pagesShared//models/category.model';
import { CategoriesService } from '@pagesShared//services/categories.service';

@Component({
  selector: 'mt-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.scss'],
})
export class AddCategoryComponent {
  @Output() categoryAdd = new EventEmitter<Category>();

  constructor(private categoriesService: CategoriesService) {}

  onSubmit(form: NgForm) {
    const { name, capacity } = form.value;
    const category = new Category(name, capacity);
    this.categoriesService.addCategory(category).subscribe(response => {
      this.resetForm(form);
      this.categoryAdd.emit(response);
    });
  }

  preventMinus(): boolean {
    return false;
  }

  private resetForm(form: NgForm) {
    form.reset();
    form.form.patchValue({
      capacity: 1,
    });
  }
}
