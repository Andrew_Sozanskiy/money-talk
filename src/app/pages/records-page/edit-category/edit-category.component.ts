import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';

import { Category } from '@pagesShared//models/category.model';
import { CategoriesService } from '@pagesShared//services/categories.service';

@Component({
  selector: 'mt-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.scss'],
})
export class EditCategoryComponent implements OnInit {
  @Input() categories: Array<Category> = [];

  @Output() categoryEdit = new EventEmitter<Category>();

  currentCategoryId = 1;
  currentCategory?: Category;

  constructor(private categoriesService: CategoriesService) {}

  ngOnInit() {
    this.findAndSetCurrentCategory();
  }

  onSubmit(form: NgForm) {
    const { capacity, name } = form.value;
    const category = new Category(
      name,
      capacity,
      this.currentCategoryId
    ) as Required<Category>;
    // todo: use toPromise() ?
    this.categoriesService.updateCategory(category).subscribe(response => {
      this.categoryEdit.emit(response);
      form.form.markAsPristine();
      // todo: add ang material toaster??
    });
  }

  onCategoryChange() {
    this.findAndSetCurrentCategory();
  }

  preventMinus(): boolean {
    return false;
  }

  private findAndSetCurrentCategory() {
    this.currentCategory = this.categories.find(category => {
      return category.id === +this.currentCategoryId;
    });
  }
}
