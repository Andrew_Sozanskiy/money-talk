import { Component, OnInit } from '@angular/core';

import { Category } from '@pagesShared//models/category.model';
import { CategoriesService } from '@pagesShared//services/categories.service';

@Component({
  selector: 'mt-records-page',
  templateUrl: './records-page.component.html',
  styleUrls: ['./records-page.component.scss'],
})
export class RecordsPageComponent implements OnInit {
  categories: Array<Category> = [];

  isLoaded = false;

  constructor(private categoriesService: CategoriesService) {}

  ngOnInit() {
    this.categoriesService.getCategories().subscribe(response => {
      this.categories = response;
      this.isLoaded = true;
    });
  }

  categoryAdded(category: Category) {
    this.categories.push(category);
  }

  categoryEdited(editedCategory: Category) {
    const editedCategoryIndex = this.categories.findIndex(category => {
      return category.id === editedCategory.id;
    });
    this.categories[editedCategoryIndex] = editedCategory;
  }
}
