import { Component, HostBinding } from '@angular/core';

import { fadeStateTrigger } from '@globalShared//animations/fade.animation';

@Component({
  selector: 'mt-auth',
  templateUrl: './auth.component.html',
  animations: [fadeStateTrigger],
})
export class AuthComponent {
  @HostBinding('@fade') a = true;
}
