import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { UserModel } from '../../shared/models/user.model';
import { UsersService } from '../../shared/services/users.service';

@Component({
  selector: 'mt-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
})
export class RegistrationComponent implements OnInit {
  form: FormGroup;

  constructor(private userService: UsersService, private router: Router) {}

  ngOnInit() {
    this.form = new FormGroup({
      email: new FormControl(
        null,
        [Validators.required, Validators.email],
        this.validateEmailNotTaken.bind(this)
      ),
      password: new FormControl(null, [
        Validators.required,
        Validators.minLength(6),
      ]),
      name: new FormControl(null, [Validators.required]),
      terms: new FormControl(false, [Validators.requiredTrue]),
    });
  }

  onSubmit() {
    const { email, password, name } = this.form.value;
    const newUser = new UserModel(email, password, name);
    this.userService.createNewUser(newUser).subscribe(() => {
      this.router.navigate(['/login'], {
        queryParams: {
          nowCanLogin: true,
        },
      });
    });
  }

  validateEmailNotTaken(
    control: AbstractControl
  ): Observable<{ emailTaken: boolean } | null> {
    return this.userService.getUserByEmail(control.value).pipe(
      map(res => {
        return res ? { emailTaken: true } : null;
      })
    );
  }
}
