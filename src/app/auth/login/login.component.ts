import { Component, HostBinding, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { MessageModel } from '../../shared/models/message.model';
import { UserModel } from '../../shared/models/user.model';
import { AuthService } from '../../shared/services/auth.service';
import { UsersService } from '../../shared/services/users.service';
import { fadeStateTrigger } from '@globalShared//animations/fade.animation';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'mt-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [fadeStateTrigger],
})
export class LoginComponent implements OnInit {
  @HostBinding('@fade') a = true;

  form: FormGroup;
  message: MessageModel = new MessageModel('danger', '');

  constructor(
    private userService: UsersService,
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private title: Title,
    private meta: Meta
  ) {
    title.setTitle('Login');
    meta.addTags([
      { name: 'keywords', content: 'login' },
      { name: 'description', content: 'page for login ' },
    ]);
  }

  ngOnInit() {
    this.route.queryParams.subscribe((params: Params) => {
      if (params['nowCanLogin']) {
        this.showMessage('Now you can login.', 'success');
      } else if (params['accessDenied']) {
        this.showMessage('you need to login', 'warning');
      }
    });
    this.form = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [
        Validators.required,
        Validators.minLength(6),
      ]),
    });
  }

  onSubmit() {
    const formData = this.form.value;
    this.userService
      .getUserByEmail(formData.email)
      .subscribe((user: UserModel) => {
        if (user) {
          if (user.password === formData.password) {
            this.showMessage('');
            this.authService.login(user);
            this.router.navigate(['mt', 'bill']);
          } else {
            this.showMessage('Wrong password');
          }
        } else {
          this.showMessage('wrong credentials');
        }
      });
  }

  private showMessage(text: string, type: string = 'danger') {
    this.message = new MessageModel(type, text);
    setTimeout(() => {
      this.message.text = '';
    }, 5000);
  }
}
