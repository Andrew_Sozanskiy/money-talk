export const environment = {
  production: true,
  endpoints: {
    apiUrl: 'http://localhost:3000/',
    exchangeApiUrl: 'https://api.exchangeratesapi.io/',
  },
};
